# Profile About Page 😃


## A stunning, responsive and clean profile page about page template 

A clean design project that I decided to develop, to be used by everyone who is interested.
Feel free to bring your ideas to contribute to this project.

Browse to page : **[click here](https://eliezerbasubiprofile.netlify.com/)**

## Tools and Technologies 

✔️ HTML

✔️ CSS

✔️ Sass

✔️ JavaScript(Vanilla)

✔️ Webpack


## How To Use

From your command line, clone and run el_ba:

```bash
# Clone this repository
$ git clone 

# Go into the repository
$ cd el_ba

# Remove current origin repository
$ git remote remove origin

# Install dependencies
$ npm install

# Start development server
$ npm start
