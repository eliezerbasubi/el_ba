const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const TerlerPlugin = require("terser-webpack-plugin");

const merge = require("webpack-merge");
const path = require("path");

const TARGET = process.env.npm_lifecycle_event;

const common = {
  entry: {
    main: "./src/index.js"
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        use: ["html-loader"]
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: {
          loader: "file-loader",
          options: {
            name: "[name].[contenthash].[ext]",
            outputPath: "images/",
            publicPath: "images/",
            esModule: false
          }
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["env"]
          }
        }
      }
    ]
  }
};

if (TARGET === "start") {
  module.exports = merge(common, {
    mode: "development",
    plugins: [
      new HtmlWebpackPlugin({
        template: "./src/index.html"
      })
    ],
    module: {
      rules: [
        {
          test: /\.(sa|sc|c)ss$/,
          exclude: /node_modules/,
          use: ["style-loader", "css-loader", "sass-loader"]
        }
      ]
    },
    output: {
      filename: "main.bundle.js",
      path: path.resolve(__dirname, "dist")
    }
  });
}

if (TARGET === "build") {
  module.exports = merge(common, {
    mode: "production",
    plugins: [
      new MiniCssExtractPlugin({ filename: "[name].[contentHash].css" }),
      new CleanWebpackPlugin()
    ],
    output: {
      filename: "main.[contentHash].bundle.js",
      path: path.resolve(__dirname, "dist")
    },
    optimization: {
      minimizer: [
        new OptimizeCssAssetsPlugin(),
        new TerlerPlugin(),
        new HtmlWebpackPlugin({
          template: "./src/index.html",
          minify: {
            removeAttributeQuotes: true,
            collapseWhitespace: true,
            removeComments: true
          }
        })
      ]
    },
    module: {
      rules: [
        {
          test: /\.(sa|sc|c)ss$/,
          exclude: /node_modules/,
          use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"]
        }
      ]
    }
  });
}
