import "./css/main.scss";
import "./js/tabs.js";
import "./js/popup.js";

export const userData = {
  name: "Jessica Parker",
  phone: "(949) 325-68594",
  address: "Newport Beach, CA",
  website: "www.seller.com"
};

export const $ = selector => {
  return document.querySelector(selector);
};

export const $$ = selector => {
  return document.querySelectorAll(selector);
};

export const loadUserInfo = () => {
  const username = $$(".profile__info--details-username");
  const user_address = $$(".profile__info-details-address");
  const phone = $$(".profile__info-details-phone");
  const web = $(".profile__info--details-web");

  // Bind data
  web.innerText = userData.website;
  for (let index = 0; index < username.length; index++) {
    username[index].innerText = userData.name;
    user_address[index].innerText = userData.address;
    phone[index].innerText = userData.phone;
  }
};

// Edit save and cancel buttons
const swapToEditPanel = () => {
  $('.about__edit__icon--btn').addEventListener('click', ()=>{
      $('.about__edit--desktop').style.display = "none";
      $('.about__edit--mobile').style.display = "block";
      $('.about__edit__icon--btn').style.display = "none";
      $('.about__edit--actions').style.display = "block";
  });

  $('#btn__secondary--edit-cancel').addEventListener('click', ()=>{
    $('.about__edit--desktop').style.display = "block";
    $('.about__edit--mobile').style.display = "none";
    $('.about__edit__icon--btn').style.display = "block";
    $('.about__edit--actions').style.display = "none";
  });
}

const powerUpUI = () => {
  loadUserInfo();
  swapToEditPanel();
};

const runApp = () => {
  powerUpUI();
};

runApp();
