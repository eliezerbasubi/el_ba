import { $ } from '../index';

const get_tab_panel = document.querySelector('.tab');

let i = 0, iterator = 0 ;
let tabPanels = document.querySelectorAll(".tab__panel");
let tabViewList = document.querySelectorAll(".tabView__list--menu-item");

const tabNavigator = (evt, tab__panel) => {
    for (i = 0; i < tabPanels.length; i++) {
      tabPanels[i].style.display = "none";
    }
    
    for (i = 0; i < tabPanels.length; i++) {
      tabViewList[i].className = tabViewList[i].className.replace(" current", "");
    }
    $(`#${tab__panel}`).style.display = "block";
    evt.currentTarget.className += " current";
}

const openTabContent = (evt, tab__panel) => {
   tabNavigator(evt, tab__panel);
}

// Bind swap event
get_tab_panel.addEventListener('touchstart', (event) =>handleTouchStart(event), false);
get_tab_panel.addEventListener('touchmove', (event)=>handleTouchMove(event), false);

let xDown = null;                                                        
let yDown = null;

const getTouches = (evt) =>{
  return evt.touches ||             
         evt.originalEvent.touches; 
}                                                     

const handleTouchStart = (evt) =>{
    const firstTouch = getTouches(evt)[0];                                      
    xDown = firstTouch.clientX;                                      
    yDown = firstTouch.clientY;                                      
};                                                

const handleTouchMove = (evt) =>{
    if ( ! xDown || ! yDown ) {
        return;
    }

    let xUp = evt.touches[0].clientX;                                    
    let yUp = evt.touches[0].clientY;

    let xDiff = xDown - xUp;
    let yDiff = yDown - yUp;

    if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {
        if ( xDiff > 0 ) {
            const next = (iterator++) + 1;
            
            if(next < tabPanels.length){
              // Change current
              tabSwiper(next, true);
            } else{
              iterator = tabPanels.length - 1;
            }
            
        } else {
          let previous = 0;
          if(iterator > 0){
           previous = (--iterator);
          }
          tabSwiper(previous, false);
        }                       
    } 
    xDown = null;
    yDown = null;                                             
};

const tabSwiper = (index, isForward) => {
  const tabMenu = $('.tabView__list--menu');

    const currentTab = !isForward 
            ? tabPanels[index + 1].getAttribute('id')
            : tabPanels[index].getAttribute('id');
    
    $(`#${currentTab}`).style.display = !isForward  
            ? 'none' 
            : "block";
    
    !isForward ? $(`#${tabPanels[index].getAttribute('id')}`).style.display = 'block'
                      : $(`#${tabPanels[index-1].getAttribute('id')}`).style.display = 'none';

    // Scroll tab headers for small devices
    if(document.body.clientWidth < 400){
      const expression = isForward ? index === tabPanels.length - 2 : index === 1;
      if(expression) {
        tabMenu.style.transform = isForward ? "translateX(-50px)" : "translateX(10px)";
        tabMenu.style.overflow = "visible"; //Set visibility to last tab             
      }
    }

    for (let k = 0; k < tabPanels.length; k++) {
      tabViewList[k].className = tabViewList[k].className.replace(" current", "");
    }

    tabViewList[index].className += " current";
}


const bindClick = () => {
    for (let j = 0; j < tabPanels.length, j < tabViewList.length; j++) {
        const tabIDs = tabPanels[j].getAttribute('id');
        const btnIDs = tabViewList[j].getAttribute('id');

        document.querySelector(`#${btnIDs}`).addEventListener('click', event => {
          // Get index of the current tab.
          iterator = j;
          openTabContent(event, `${tabIDs}`);
        });
    }
}

bindClick();