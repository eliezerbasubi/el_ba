import { userData } from "../index.js";
import { loadUserInfo } from "../index.js";


let input_field_name = "";

const $$ = selector => {
  return document.querySelectorAll(selector);
};
const $ = selector => {
  return document.querySelector(selector);
};

const popup = $(".popup");
const edited_field = $('.input__border_bottom');

const openEditDialogBox = () => {
  const edit_btns = $$(".about__btn__edit");

  for (let index = 0; index < edit_btns.length; index++) {
    const btn = edit_btns[index];

    btn.addEventListener("click", event => {
      const target = event.currentTarget;
      const btnPosition = target.getBoundingClientRect();
      const top = btnPosition.top;

      popup.style.display = "block";

      popup.style.top = top + "px";

      if (index === edit_btns.length - 2) {
        popup.style.marginLeft = 15 + "px";
      }

      // clear edit input field
      edited_field.value = "";

      //   Append value to label
      const label_value = target.getAttribute("aria-data-field");
      $(".float__popup").innerText = label_value;

      //   Get Input field name
      input_field_name = target.getAttribute('name');

      // Change type of edit input dynamically
      switch (label_value) {
        case "WEBSITE":
            $('.input__border_bottom').setAttribute('type',"url");
          break;
        case "PHONE":
            $('.input__border_bottom').setAttribute('type',"url");
        default:
            $('.input__border_bottom').setAttribute('type',"url");
          break;
      }
    });
  }
};

const cancelEditDialogBox = () => {
  $("#btn__secondary--cancel").addEventListener("click", () => {
    popup.style.display = "none";
  });
};

const editUserInfo = () => {
  const btn_save = $("#btn__primary--save");
  btn_save.addEventListener("click", () => {
    if(edited_field.value !== null){
        popup.style.display = "none";

        userData[input_field_name] = edited_field.value;

        edited_field.value = "";
        loadUserInfo();
    }
  });
};

const runner = () => {
  openEditDialogBox();
  cancelEditDialogBox();
  editUserInfo();
};

runner();
